package utilities;

import static driverfactory.Driver.driver;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import ui.smoke.testcases.GHRM.TC1_GHMRVerification;


public class SikuliActions {
	public static Screen screen;
	public static Pattern pattern;
	public static String screenPath = System.getProperty("user.dir") + "/src/main/resources/WindowScreens/";

	public SikuliActions() {
		screen = new Screen();
	}

	public static void doubleClick(String screenName) throws FindFailed {
		String screenPath1 = screenPath + screenName;
		pattern = new Pattern(screenPath1);
		System.out.println(screenPath1);
		screen.doubleClick(screenPath1);
		screen.wait(pattern, 10);
	}
	
	public static void click(String screenName) throws FindFailed {
		String screenPath2 = screenPath  + screenName;
		pattern = new Pattern(screenPath2);
		screen.click(screenPath2);
		screen.wait(pattern, 10);
	}

	public static void setFilePath(String screenName, String path) throws FindFailed {
		String screenPath3 = screenPath  + screenName;
		pattern = new Pattern(screenPath3);
		screen.type(pattern, path);
		screen.wait(pattern, 10);
	}

	public static void saveFileOpenedInBrowser(String fileName, String browserType) throws FindFailed, InterruptedException {
		if(browserType.equalsIgnoreCase("ie")) 
			click("SaveImg");
		Actions builder = new Actions(driver);
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.chord("s")).build().perform();
		Thread.sleep(2000);
		doubleClick("FileNameTxt");
		setFilePath("FileNameTxt", fileName);
		click("SaveBtn");
		setFilePath("FileNameTxt", fileName);
		click("SaveBtn");
		
		}
}
