package ui.smoke.testcases.GHRM;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;
import static verify.SoftAssertions.verifyContains;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.GHRM_Pages.GHRMHeaderPage;
import pages.GHRM_Pages.GHRMReportsPage;
import pages.GHRM_Pages.HomePage;
import pages.GHRM_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;
import static utilities.PDFReader.readPDfContent;

public class TC2a_GHMRVerification extends InitTests {

	public TC2a_GHMRVerification(String appname) {
		super(appname);
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod()
	public void initializeBrowser() throws Exception {
		test = reports.createTest("GHMRVerifciation_Login");
		test.assignCategory("smoke");
		softAssert = new SoftAssert();
		TC2a_GHMRVerification test1 = new TC2a_GHMRVerification("GHMRV");
		initWebDriver(BASEURL, "chrome", "", "", "local", test, "");


	}

	@Test(priority = 1, enabled = true)
	public void COLReportTest() throws Exception {
		try {

			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""),test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Cost of Living Report");
			ghrmReportsPage.selectReportRegionAndCountry("GERMANY", "BERLIN");
			ghrmReportsPage.clickViewResultBtn();
			switchToWindowWithURL("colRepRes.php");
			ghrmReportsPage.clickReportFromTable("International & English schools");
			//String result = ghrmReportsPage.checkCOLResultTableContainsValue();
			//verifyEquals(result, "true", test);
//			ghrmReportsPage.clickViewReportsLink();
//			ghrmReportsPage.savePDFReport("COLReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "BERLIN", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(priority = 2, enabled = true)
	public void AccomodationReportTest() throws Exception {
		try {

			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Cost of Living Report");
			ghrmReportsPage.selectReportRegionAndCountry("GERMANY", "BERLIN");
			ghrmReportsPage.clickViewResultBtn();
			switchToWindowWithURL(".PDF");
//			ghrmReportsPage.savePDFReport("ACcomodationReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "BERLIN", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(priority = 3, enabled = true)
	public void QOLReportTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Quality of Living Report");
			ghrmReportsPage.selectReportRegionAndCountry("GERMANY", "BERLIN");
			ghrmReportsPage.clickViewResultBtn();
			switchToWindowWithURL("qolRepRes.php");
			ghrmReportsPage.clickReportFromTable("Political And Social Environment");
			ghrmReportsPage.clickViewReportsLink();
//			ghrmReportsPage.savePDFReport("QOLReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "BERLIN", "actual value contains expected", test);
//			verifyContains(content, "GERMANY", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(priority = 4, enabled = true)
	public void PTReportTest() throws Exception {
		try {

			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Personal Tax Report");
			switchToWindow("iMercer.com");
			verifyElementTextContains(HomePage.asiaLnk, "asia", test);
		

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@Test(priority = 5, enabled = true)
	public void WBEGReportTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Worldwide Benefit & Employment Guidelines");
			ghrmReportsPage.selectCountryFromMutliLst("Germany");
			ghrmReportsPage.clickViewResultBtn();
			ghrmReportsPage.clickViewReportsLink();
			switchToWindowWithURL(".PDF");
//			ghrmReportsPage.savePDFReport("WBEGReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "BERLIN", "actual value contains expected", test);
//			verifyContains(content, "GERMANY", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(priority = 6, enabled = true)
	public void BPPReportTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Benefits, Policies and Practice Report");
			ghrmReportsPage.selectCountryFromMutliLst("Germany");
			ghrmReportsPage.clickViewResultBtn();
			ghrmReportsPage.clickViewReportsLink();
			switchToWindowWithURL(".PDF");
//			ghrmReportsPage.savePDFReport("WBEGReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "GERMANY", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(priority = 7, enabled = true)
	public void CORReportTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickReportsLink();
			GHRMReportsPage ghrmReportsPage = new GHRMReportsPage();
			ghrmReportsPage.clickReportsLink("Compensation Overview Report");
			ghrmReportsPage.selectCountryFromMutliLst("Germany");
			ghrmReportsPage.clickViewResultBtn();
			ghrmReportsPage.clickViewReportsLink();
			switchToWindowWithURL(".PDF");
//			ghrmReportsPage.savePDFReport("WBEGReport");
//			String content = readPDfContent("Libraries\\Documents\\" + ghrmReportsPage.fileName);
//			verifyContains(content, "GERMANY", "actual value contains expected", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@AfterMethod
	public void kill() {
		driver.quit();
		killBrowserExe("chrome");
	}

}
