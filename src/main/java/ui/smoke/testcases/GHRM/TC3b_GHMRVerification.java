package ui.smoke.testcases.GHRM;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyTableContent;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.GHRM_Pages.GHRMCalculatorsPage;
import pages.GHRM_Pages.GHRMHeaderPage;
import pages.GHRM_Pages.HomePage;
import pages.GHRM_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3b_GHMRVerification extends InitTests{

	public TC3b_GHMRVerification(String appname) {
		super(appname);
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod()
	public void initializeBrowser() throws Exception {
		test = reports.createTest("GHMRVerifciation_Login");
		test.assignCategory("smoke");
		softAssert = new SoftAssert();
		TC3b_GHMRVerification test1 = new TC3b_GHMRVerification("GHMRV");
		initWebDriver(BASEURL, "chrome", "", "", "local", test, "");

	}

	@Test(priority = 1, enabled = true)
	public void SIHHNCalculatorTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickCalculatorsLink();
			GHRMCalculatorsPage ghrmCalPage = new GHRMCalculatorsPage();
			ghrmCalPage.clickCalculatorsLink("Spendable Income - Home Housing Norm and Savings");
			ghrmCalPage.enterSIHMCalculatorDetails("United States - California", "50000" );
			ghrmCalPage.clickCalculateBtn();
			switchToWindow("Home Country Net Breakdown");
			verifyTableContent(ghrmCalPage.SIHHNResultTbl, "SIHHN Result", "United States - California~5000", test);
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@AfterSuite
	public void kill() {
		driver.quit();
		killBrowserExe("chrome");
	}
}
