package ui.seleniumgrid.testcases;


import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;

import static verify.SoftAssertions.verifyElementTextContains;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TestSeleniumGrid extends InitTests {

/*
 * To test login on remote machine using selenium grid
 *
 * 
 ///Hub
//java -jar selenium-server-standalone-2.53.0.jar -role hub
//http://localhost:4444/grid/console
//Node
//java -Dwebdriver.chrome.driver=""  -jar selenium-server-standalone-2.53.0.jar -role webdriver -hub http://10.2.118.176:4444/grid/register -port 1398
//java -Dwebdriver.ie.driver="" -jar selenium-server-standalone-2.53.0.jar -role webdriver -hub http://10.2.118.176:4444/grid/register -port 1398

//Parallel 
//java -Dwebdriver.chrome.driver="" java -Dwebdriver.ie.driver="" -jar selenium-server-standalone-2.53.0.jar -role webdriver -hub http://10.2.118.176:4444/grid/register -port 1398
 */
	  Driver driverObj=new Driver();
	  WebDriver driver = null;
	  ExtentTest test=null;
	@BeforeClass
	  @Parameters({"nodeUrl"})
	public void init(String nodeUrl) throws Exception
	{
		System.out.println(nodeUrl);
		test = reports.createTest("testLogin with grid" +nodeUrl);
		test.assignCategory("Grid");
		  driver=driverObj.initWebDriver(BASEURL,"chrome","latest","WINDOWS","grid",test,nodeUrl);
	}
  @Test(priority = 1, enabled = true)
  public void testSeleniumGrid() throws IOException {
	
	  try
	  {
		  LoginPage loginpage=new LoginPage(driver);
		  loginpage.login(USERNAME, PASSWORD);
		  DashBoardPage homepage = new DashBoardPage(driver);
			waitForElementToDisplay(DashBoardPage.countryDropdwnLnk);
			verifyElementTextContains(DashBoardPage.countryDropdwnLnk, "Choose Country",test);


	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testLogin()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();


	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testLogin()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();


	} 
	finally
	{
		reports.flush();
		driver.close();

	}
  }
}
